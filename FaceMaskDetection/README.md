# Face Mask Detection

To run:
```bash
python src/detect_mask_on_video.py --input input_filename.mp4
```
or
```bash
python src/detect_mask_on_video.py --input input_filename.mp4 --output output_filename.avi
```

### Data Collection

- Faces with Mask - Dataset
    - [Kaggle Medical Mask Dataset](https://www.kaggle.com/ivandanilovich/medical-masks-dataset-images-tfrecords)
    - [MaFa - Masked Faces](https://drive.google.com/drive/folders/1nbtM1n0--iZ3VVbNGhocxbnBGhMau_OG)
- Faces without Mask - Dataset
    - [FDDB Dataset](http://vis-www.cs.umass.edu/fddb/)
    - [Wider Face Dataset](http://shuoyang1213.me/WIDERFACE/)

All Data collection codes are available as Jupyter Notebooks in `notebooks` folder.

### Training on Google Colab

For Implemnetation, I have used following Research papers as references:

Research Paper Link : [Face Mask Detection Using MobileNetV2 in The Era of COVID-19 Pandemic](https://ieeexplore.ieee.org/document/9325631)

Colab Link : [https://colab.research.google.com/drive/1VuaDyXq9FIFfYyF_4V598Sg4wDS4mM2u?usp=sharing](https://colab.research.google.com/drive/1VuaDyXq9FIFfYyF_4V598Sg4wDS4mM2u?usp=sharing)

The model was saved in the `.h5` format having file size of 11MB.

`
Note: I have collected nearly 93K+ Masked and Unmasked images but I have used only 10K images (5K from Masked & 5K from Unmasked) for Training Mask detection model because of less computational resource on colab.
`

### Implementing Trained Model on Real-World Systems.

Model configurations are present in `config.py` file.
Model was implemented in `detect_mask_on_video.py` file.

To Setup environment:
```bash
git clone https://gitlab.com/ashok.ai/ajna-deep-learning-task.git
cd ajna-deep-learning-task/FaceMaskDetection
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

To run:
```bash
python src/detect_mask_on_video.py --input input_filename.mp4
```
or
```bash
python src/detect_mask_on_video.py --input input_filename.mp4 --output output_filename.avi
```