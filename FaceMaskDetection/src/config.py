"""Mask Detection Configurations."""

FACENET_PROTOTXT_PATH = "src/face_detector/deploy.prototxt.txt"
FACENET_WEIGHTS_PATH = "src/face_detector/res10_300x300_ssd_iter_140000.caffemodel"
MASKNET_NODEL_PATH = "src/trained_models/model-015-0.956944-0.946317.h5"