"""Detect Mask on People Faces.

Usage:

python detect_mask_on_video.py --input input_filename.mp4
python detect_mask_on_video.py --input input_filename.mp4 --output output_filename.avi

"""

# Importing Required Packages
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from cv2 import cv2
import numpy as np
import argparse
import config

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", type=str, default="",
	help="path to (optional) input video file")
ap.add_argument("-o", "--output", type=str, default="",
	help="path to (optional) output video file")
ap.add_argument("-d", "--display", type=int, default=1,
	help="whether or not output frame should be displayed")
args = vars(ap.parse_args())

def detect_and_predict_mask(frame, faceNet, maskNet):
	"""Detect Faces from frame and Predicts mask status on detected faces.

	Args:
		frame : Frame to be processed
		faceNet : To detect faces in the frame
		maskNet : To predict mask status of faces detected

	Returns:
		Location of Faces and Mask Status of recpected faces.
	"""
	# Extracting the dimensions of the frame and then construct a blob from it
	(h, w) = frame.shape[:2]
	blob = cv2.dnn.blobFromImage(frame, 1.0, (224, 224), (104.0, 177.0, 123.0))

	# pass the blob through the network and obtain the face detections
	faceNet.setInput(blob)
	detections = faceNet.forward()  # Detecting Faces on the frame

	# initialize our list of faces, their corresponding locations,
	# and the list of predictions from our face mask network
	faces = []
	locs = []
	predictions = []

	# loop over the detections
	for i in range(0, detections.shape[2]):
		# extract the confidence (i.e., probability) associated with
		# the detection
		confidence = detections[0, 0, i, 2]

		# filter out weak detections by ensuring the confidence is
		# greater than the minimum confidence
		if confidence > 0.2:
			# compute the (x, y)-coordinates of the bounding box for
			# the object
			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")

			# ensure the bounding boxes fall within the dimensions of
			# the frame
			(startX, startY) = (max(0, startX), max(0, startY))
			(endX, endY) = (min(w - 1, endX), min(h - 1, endY))

			# extract the face ROI, convert it from BGR to RGB channel
			# ordering, resize it to 224x224, and preprocess it
			face = frame[startY:endY, startX:endX]
			# print(type(face), face.shape, face)
			face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
			face = cv2.resize(face, (224, 224))
			face = img_to_array(face)
			face = preprocess_input(face)

			# add the face and bounding boxes to their respective
			# lists
			faces.append(face)
			locs.append((startX, startY, endX, endY))

	# only make a predictions if at least one face was detected
	if len(faces) > 0:
		# for faster inference we'll make batch predictions on *all*
		# faces at the same time rather than one-by-one predictions
		# in the above `for` loop
		faces = np.array(faces, dtype="float32")
		predictions = maskNet.predict(faces, batch_size=32)

	# return a 2-tuple of the face locations and their corresponding
	# locations
	return (locs, predictions)

# load our serialized face detector model from disk

prototxtPath = config.FACENET_PROTOTXT_PATH
weightsPath = config.FACENET_WEIGHTS_PATH
faceNet = cv2.dnn.readNet(prototxtPath, weightsPath)

# load the face mask detector model from disk
maskNet = load_model(config.MASKNET_NODEL_PATH)

# initialize the video
print("[INFO] Loading Video...")
video_path = args["input"] if args["input"]!="" else 0
video_captured = cv2.VideoCapture(video_path)

# Check if camera opened successfully
if video_captured.isOpened() == False:
    print("Error opening video stream or file")
    
frame_width = int(video_captured.get(3))
frame_height = int(video_captured.get(4))

if args["output"]:
    output_file = cv2.VideoWriter(args["output"], cv2.VideoWriter_fourcc('M','J','P','G'), 30, 
                                  (frame_width,frame_height))

# loop over the frames from the video stream
while video_captured.isOpened():
	# grab the frame from the threaded video stream and resize it
	# to have a maximum width of 400 pixels
	ret, frame = video_captured.read()
	if ret == True:
		# detect faces in the frame and determine if they are wearing a
		# face mask or not
		(locs, preds) = detect_and_predict_mask(frame, faceNet, maskNet)

		# loop over the detected face locations and their corresponding
		# locations
		for (box, pred) in zip(locs, preds):
			# unpack the bounding box and predictions
			(startX, startY, endX, endY) = box
			(mask, withoutMask) = pred

			# determine the class label and color we'll use to draw
			# the bounding box and text
			label = "Mask" if mask > withoutMask else "No Mask"
			color = (0, 255, 0) if label == "Mask" else (0, 0, 255)

			# include the probability in the label
			label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)

			# display the label and bounding box rectangle on the output
			# frame
			cv2.putText(frame, label, (startX, startY - 10),
				cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)
			cv2.rectangle(frame, (startX, startY), (endX, endY), color, 2)

		# show the output frame
		if args["output"] != "":
			output_file.write(frame)
		if args["display"] == 1:
			cv2.imshow("Frame", frame)
			key = cv2.waitKey(1) & 0xFF

		# if the `q` key was pressed, break from the loop
		if key == ord("q"):
			break
	else:
		break

# do a bit of cleanup
cv2.destroyAllWindows()
video_captured.release()
output_file.release()